# Flectra Community / storage

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[storage_image_product](storage_image_product/) | 2.0.3.3.0| Link images to products and categories
[storage_import_image_advanced](storage_import_image_advanced/) | 2.0.1.0.2| Import product images using CSV
[storage_image_product_brand](storage_image_product_brand/) | 2.0.1.3.0| Link images to product brands
[storage_image_import](storage_image_import/) | 2.0.1.2.0| Add the possibility to import image for csv base on url
[storage_backend_s3](storage_backend_s3/) | 2.0.2.0.1| Implement amazon S3 Storage
[storage_backend_ftp](storage_backend_ftp/) | 2.0.1.0.3| Implement FTP Storage
[storage_media](storage_media/) | 2.0.2.1.0| Give the posibility to store media data in Odoo
[storage_image_product_pos](storage_image_product_pos/) | 2.0.1.1.0| Link images to products and categories inside POS
[storage_media_product](storage_media_product/) | 2.0.2.1.0| Link media to products and categories
[storage_image_product_import](storage_image_product_import/) | 2.0.1.0.0| Helper for importing image for csv base on url
[storage_file](storage_file/) | 2.0.2.3.0| Storage file in storage backend
[storage_image_category_pos](storage_image_category_pos/) | 2.0.1.0.0| Add image handling to product category and use it for POS
[storage_backend](storage_backend/) | 2.0.2.0.2| Implement the concept of Storage with amazon S3, sftp...
[storage_backend_sftp](storage_backend_sftp/) | 2.0.2.0.2| Implement SFTP Storage
[storage_image_product_brand_import](storage_image_product_brand_import/) | 2.0.1.0.1| Helper for importing image for csv base on url
[storage_thumbnail](storage_thumbnail/) | 2.0.2.3.0| Abstract module that add the possibility to have thumbnail
[storage_image](storage_image/) | 2.0.2.2.0| Store image and resized image in a storage backend


